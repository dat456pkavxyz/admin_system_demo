$(function () {
    USER.init();
    PRODUCT.init();

})
// For Product
const PRODUCT = {
    init: function () {
        PRODUCT.addP();
        PRODUCT.deleteP();
    },

    addP: function () {
        const submit = $(".btn_addP");
        submit.on("click", function (e) {
            e.preventDefault();
            const name = $("#val-name").val();
            const des = CKEDITOR.instances["val-des"].getData();
            const discount = $("#val-discount").val();
            const price = $("#val-price").val();
            const stock = $("#val-stock").val();
            const status = $("#val-status").val();
            const supplier = $("#val-sup").val();
            const fileUpload = $("#val-img")[0].files[0]; // Lấy tệp tin tải lên

            let dataObj = {
                name: name,
                description: des,
                discount: discount,
                price: price,
                stock: stock,
                status: status,
                sup: supplier,
            };
            console.log(dataObj);
            var formData = new FormData(document.getElementById("myFormP"));
            formData.append("fileUpload", fileUpload); // Thêm tệp tin vào FormData
            formData.append("data", JSON.stringify(dataObj)); // Thêm chuỗi JSON vào FormData


            // console.log(dataObj);
            $.ajax({
                url: "http://localhost/admin_system_demo/admin/productController/insert",
                type: "POST",
                data: formData, // Truyền FormData
                contentType: false, // Vô hiệu hóa contentType để trình duyệt tự động đặt giá trị
                processData: false, // Vô hiệu hóa processData để trình duyệt không xử lý dữ liệu
                success: function (data) {
                    if (data) {
                        location.assign('http://localhost/admin_system_demo/admin/productController/index');
                    } else {
                        alert("Lỗi Rồi");
                    }
                },
                error: function () {
                    alert("Lỗi Rồi");
                }
            });
        });
    },

    deleteP: function () {
        const deletebtn = $(".btn-deleteP");
        const input_id = $(".product-id")
        for (let i = 0; i < deletebtn.length; i++) {
            $(deletebtn[i]).on("click", function (e) {
                e.preventDefault();
                let ojb = {
                    id: $(input_id[i]).val(),
                };
                console.log(ojb);
                $.ajax({
                    url: `http://localhost/admin_system_demo/admin/productController/delete`,
                    type: 'POST',
                    data: ojb,
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            alert(data.messages); // Hiển thị thông báo thành công
                            location.reload(); // Làm mới trang nếu xóa thành công
                        } else {
                            alert(data.messages); // Hiển thị thông báo lỗi nếu xóa không thành công
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        // Xử lý lỗi kết nối
                        alert("Lỗi kết nối");
                    }
                    
                })
            })
        }
    },

};
//For User
const USER = {
    init: function () {
        USER.add();
        USER.delete();
        USER.update();
        USER.change();
        USER.search();
    },
    add: function () {

        const submit = $(".btn_add");


        submit.on("click", function (e) {
            e.preventDefault();

            var formData = new FormData(document.getElementById("myForm"));

            $.ajax({
                url: `http://localhost/admin_system_demo/admin/userController/insert`,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    console.log(data)
                    if (data) {
                        location.assign('http://localhost/admin_system_demo/admin/userController/index');
                    }
                    else alert("LỖi RỒi")

                }

            })
        });

    },
    delete: function () {
        const deletebtn = $(".btn-delete");
        const input_id = $(".user-id")

        for (let i = 0; i < deletebtn.length; i++) {
            $(deletebtn[i]).on("click", function (e) {
                e.preventDefault();
                let ojb = {
                    id: $(input_id[i]).val(),
                };
                $.ajax({
                    url: `http://localhost/admin_system_demo/admin/userController/delete`,
                    type: 'POST',
                    data: ojb,
                    success: function (data) {
                        if (data) {

                            location.reload();
                        }
                        else alert("LỖi RỒi")

                    }
                })
            })


        }

    },

    update: function () {
        const btnUpdate = $('.btn-update');
        btnUpdate.on("click", function (e) {
            e.preventDefault();

            const id = $("#val-id").val();
            const name = $("#val-name").val();
            const email = $("#val-email").val();
            const phone = $("#val-phone").val();
            const permission = $("#val-permission").val();
            const address = $("#val-address").val();
            const password = $("#val-password").val();
            const avatar = $('#val-img').val();



            let dataOjb = {
                id: id,
                name: name,
                email: email,
                phone: phone,
                avatar: avatar,
                permission: permission,
                address: address,
                password: password,
                type: "updateUser"
            }
            console.log(dataOjb);
            $.ajax({
                url: `http://localhost/admin_system_demo/admin/userController/update`,
                type: 'POST',
                data: dataOjb,
                success: function (data) {
                    console.log(data);
                    if (data) {
                        location.assign('http://localhost/admin_system_demo/admin/userController/index');
                    }
                    else alert("LỖi RỒi")

                }

            })
        });

    },

    change: function () {
        $('#avatar-upload').change(function (e) {
            e.preventDefault();
            var id = $('#val-id').val(); // lấy giá trị của id
            var formData = new FormData(document.getElementById("myForm")); // tạo FormData object và đẩy lên server
            formData.append('id', id);
            formData.append('fileUpload', $('#avatar-upload')[0].files[0]);

            $.ajax({
                url: 'http://localhost/admin_system_demo/admin/userController/update',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    if (data) {
                        location.assign('http://localhost/admin_system_demo/admin/userController/viewUpdate/' + id);
                    } else {
                        alert("LỖi RỒi");
                    }
                }
            });
        });
    },
    search: function () {
        const btn_search = $("#searchInforUser");
        btn_search.on("click", function (e) {
            e.preventDefault();
            const keyword = $("#keyword").val();
            let dataOjb = {
                keyword: keyword,
                type: "getUserKey"
            }
            console.log(dataOjb);
            $.ajax({
                url: 'http://localhost/admin_system_demo/admin/userController/search',
                type: 'POST',
                data: dataOjb,
                dataType: "json",
                encode: true,
                success: function (data) {
                    if (data) {
                        sessionStorage.setItem('dataOjb', JSON.stringify(data));
                        console.log(data);
                        location.assign('http://localhost/admin_system_demo/admin/userController/index/?type=getUserKey&&keyword=' + keyword);
                    } else {
                        alert("LỖi RỒi");
                    }
                }
            });
        });
    }

    // search: function(){
    //     const btn_search = $("#searchInforUser");
    //     btn_search.on("click", function (e) {
    //       e.preventDefault();
    //       const keyword = $("#keyword").val();
    //       let dataOjb = {
    //         keyword: keyword,
    //         type: "getUserKey"
    //       }
    //       console.log(dataOjb);
    //       $.ajax({
    //         url: 'http://localhost/admin_system_demo/admin/userController/search',
    //         type: 'POST',
    //         data: dataOjb,
    //         dataType: "json",
    //         encode: true,
    //         success: function(data) {
    //           if (data) {
    //             sessionStorage.setItem('dataOjb', JSON.stringify(data));
    //             console.log(data);
    //             location.assign('http://localhost/admin_system_demo/admin/userController/index/?type=getUserKey&&keyword='+keyword);
    //           } else {
    //               alert("LỖi RỒi");
    //           }
    //         }
    //       });
    //     });
    //   }
}
