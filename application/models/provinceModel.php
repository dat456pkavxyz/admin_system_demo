<?php 
class provinceModel extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }
    public function getProvince(){
        $this->db->order_by('name', 'ASC'); //sắp xếp name theo ký tự ASC
        $res = $this->db->get('tbl_province');
        return $res;
    }
    public function getProvinceName($provinceid){
        $this->db->where('id', $provinceid);
        $row = $this->db->get('tbl_province')->row_array();
        return $row['name'];
    }
}
?>