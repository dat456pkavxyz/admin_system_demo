<?php
class userModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    public function getAllUser()
    {
        $res =  $this->db->get('tbl_users');
        return $res;
    }
    public function addUser($data)
    {
        $res = $this->db->insert('tbl_users', $data);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteUser($id)
    {
        $this->db->where('id',$id);
        $res = $this->db->delete('tbl_users');
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
    public function updateUser($id,$data){
        $this->db->where('id',$id);
        $res = $this->db->update('tbl_users',$data);
        return $res;
    }
    public function getbyId($id){
        $this->db->where('id',$id);
        $res =  $this->db->get('tbl_users');
        return $res;
    }
    public function countUser(){
        $query =  $this->db->get('tbl_users');
        $res = $query->num_rows();
        return $res;
    }
    public function total_a_page($offset, $limit){
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tbl_users');
        return $query->result_array();
    }
    public function searchUser($result){
        $this->db->select('tbl_users.*');
        $this->db->from('tbl_users');
        $this->db->join('tbl_province', 'tbl_province.id = tbl_users.address', 'left');
        $this->db->like('tbl_users.name', $result, 'both');
        $this->db->or_like('tbl_province.name', $result, 'both');
        $res =  $this->db->get();
        return $res;
    }
    
    
    
}
