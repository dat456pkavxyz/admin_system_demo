<?php
class productModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    public function getAllProduct()
    {
        $res =  $this->db->get('tbl_products');
        return $res;
    }
    public function addProduct($data)
    {
        $res = $this->db->insert('tbl_products', $data);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
    public function getLastInsertedID($data)
    {
        $productID = $this->db->insert_id();
        return $productID;
    }
    public function addProductIMG($data)
    {
        $res = $this->db->insert('tbl_productimage', $data);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteProduct($id)
    {
        $this->db->where('id', $id);
        $res = $this->db->delete('tbl_products');
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
    public function updateProduct($id, $data)
    {
        $this->db->where('id', $id);
        $res = $this->db->update('tbl_products', $data);
        return $res;
    }
    public function getbyIdProduct($id)
    {
        $this->db->where('id', $id);
        $res =  $this->db->get('tbl_products');
        return $res;
    }
    public function countProduct()
    {
        $query =  $this->db->get('tbl_products');
        $res = $query->num_rows();
        return $res;
    }
    public function total_a_pageProduct($offset, $limit)
    {
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tbl_products');
        return $query->result_array();
    }
    public function getAllImage(){
        $res =  $this->db->get('tbl_productimage');
        return $res;
    }
}
