
<!-- header and sidebar-->
<?php $this->load->view('layout/header.php')   ?>
<!-- end header and sidebar-->
<script src="https://cdn.ckeditor.com/4.21.0/standard/ckeditor.js"></script>

<!-- content -->
<?php $this->load->view($subview)   ?>
<!-- end content -->

<!-- footer -->
<?php $this->load->view('layout/footer.php')   ?>
<!-- end footer -->

