<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('public/images/logoo.png') ?>">
    <title>login</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('public/css/lib/bootstrap/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href=" <?= base_url('public/css/helper.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/css/style.css') ?>" rel="stylesheet">
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <!-- Main wrapper  -->
    <div id="main-wrapper">

        <div class="unix-login">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="login-content card">
                            <div class="login-form">
                                <h4>Login Admin</h4>
                                <form action="login" method="POST">
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email" name="email" class="form-control" placeholder="Email">
                                        <div class="error" id="password_error"><?php echo form_error('name') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" class="form-control" placeholder="Password">
                                    </div>

                                    <button type="submit" name="login" class="btn btn-primary btn-flat m-b-30 m-t-30">Sign in</button>

                                    <?php if (isset($error)) : ?>
                                        <div class="row-error">
                                            <?php echo "<p  style='color:red;'>$error</p>"; ?>
                                        </div>
                                    <?php endif; ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->

 

</body>

</html>
