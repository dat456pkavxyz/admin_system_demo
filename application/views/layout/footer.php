<footer class="footer"> Thank for watching</a></footer>
</div>
<!-- End Wrapper -->
<!-- All Jquery -->

<script src="https://cdn.ckeditor.com/4.21.0/standard/ckeditor.js"></script>
<script src="<?= base_url('public/js/custom.js') ?>"></script>

<script src="<?= base_url('public/js/lib/jquery/jquery.min.js') ?>"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?= base_url('public/js/lib/bootstrap/js/popper.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/bootstrap/js/bootstrap.min.js') ?>"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?= base_url('public/js/jquery.slimscroll.js') ?>"></script>
<!--Menu sidebar -->
<script src="<?= base_url('public/js/sidebarmenu.js') ?>"></script>
<!--stickey kit -->
<script src="<?= base_url('public/js/lib/sticky-kit-master/dist/sticky-kit.min.js') ?>"></script>
<!--Custom JavaScript -->
<!-- Amchart -->
<script src="<?= base_url('public/js/lib/morris-chart/raphael-min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/morris-chart/morris.js') ?>"></script>
<script src="<?= base_url('public/js/lib/morris-chart/dashboard1-init.js') ?>"></script>


<script src="<?= base_url('public/js/lib/calendar-2/moment.latest.min.js') ?>"></script>
<!-- scripit init-->
<script src="<?= base_url('public/js/lib/calendar-2/semantic.ui.min.js') ?>"></script>
<!-- scripit init-->
<script src="<?= base_url('public/js/lib/calendar-2/prism.min.js') ?>"></script>
<!-- scripit init-->
<script src="<?= base_url('public/js/lib/calendar-2/pignose.calendar.min.js') ?>"></script>
<!-- scripit init-->
<script src="<?= base_url('public/js/lib/calendar-2/pignose.init.js') ?>"></script>

<script src="<?= base_url('public/js/lib/owl-carousel/owl.carousel.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/owl-carousel/owl.carousel-init.js') ?>"></script>
<script src="<?= base_url('public/js/scripts.js') ?>"></script>
<!-- scripit init-->
<script src="<?= base_url('public/js/custom.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/form-validation/jquery.validate.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/form-validation/jquery.validate-init.js') ?>"></script>

<script src="<?= base_url('public/js/lib/datatables/datatables.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') ?>"></script>
<script src="<?= base_url('public/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') ?>"></script>
<script src="<?= base_url('public/js/lib/datatables/datatables-init.js') ?>"></script>

</body>

</html>