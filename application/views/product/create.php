<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Manage Products</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url() . 'admin/productController' ?>">List Product</a></li>
                <li class="breadcrumb-item active">Create Product</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-validation">
                            <form class="form-valide " id="myFormP" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class=" col-lg-7 form-group form-group-vertical">
                                        <label for="val-name">Product Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="val-name" name="name" placeholder="Enter a productname...">
                                    </div>
                                    <div class="col-lg-4 form-group form-group-vertical">
                                        <label for="val-password">Price<span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" id="val-price" name="price" placeholder="Enter price.." min="0">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class=" col-lg-7 form-group form-group-vertical">
                                        <label for="val-name">Supplier<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="val-sup" name="sup" placeholder="Enter supplier..">
                                    </div>
                                    <div class="col-lg-4 form-group form-group-vertical">
                                        <label for="val-password">In Stock<span class="text-danger">*</span></label>
                                        <input type="number" class="form-control" id="val-stock" name="stock" placeholder="Enter in stock.." min="0">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-group">
                                            <label class="col-form-label" for="val-des">Description <span class="text-danger">*</span></label>
                                            <textarea name="val-des" id="val-des" placeholder="Enter description.."></textarea>
                                            <script>
                                                CKEDITOR.replace('val-des');
                                            </script>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="d-flex flex-column">
                                            <div class="form-group">
                                                <label for="val-discount">Discount<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" id="val-discount" name="discount" placeholder="Enter ..." min="0">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-form-label" for="val-email">Status<span class="text-danger">*</span></label>
                                                <select class="form-control" name="status" id="val-status">
                                                    <!-- <option value="">Please select</option> -->
                                                    <option value="1">Kinh Doanh</option>
                                                    <option value="0">Chưa Kinh Doanh</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="val-password">Image Main<span class="text-danger">*</span></label>
                                                <input type="file" id="val-img" name="fileUpload">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-lg-7 ml-auto">
                                <button class="btn btn-primary btn_addP">Submit</button>
                                <button type="button" class="btn btn-danger">
                                    <a href="<?= base_url() . 'admin/productController' ?>" class="text-white">Cancel</a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->

</div>

<style>
    .dropzone .dz-preview img {
        max-width: 300px;
        max-height: 225px;
        width: auto;
        height: auto;
    }

    .dropzone .dz-preview .dz-image {
        border-radius: 5px;
        overflow: hidden;
        width: 120px;
        height: 120px;
        margin-right: 10px;
    }
</style>