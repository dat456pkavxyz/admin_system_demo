<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Update User</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Manage Users</a></li>
                <li class="breadcrumb-item active">Update User</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-validation">
                            <?php foreach ($dataUser as $user) : extract($user); ?>

                                <form class="form-valide" id="myForm" enctype="multipart/form-data">

                                    <input type="hidden" class="user-id" id="val-id" value="<?= $user['id'] ?>" name="id" placeholder="Enter a username..">
                                    <div class="form-group row">

                                        <label class="col-lg-2 col-form-label" for="val-username">Username <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="text" class="form-control" id="val-name" value="<?= $name ?>" name="name" placeholder="Enter a username..">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="val-email">Email <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="email" class="form-control" id="val-email" value="<?= $email ?>" name="email" placeholder="Your valid email..">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="val-suggestions">Phone <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input class="form-control" type="text" id="val-phone" value="<?= $phone ?>" name="phone" rows="5" placeholder="phone...">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="val-suggestions">Avatar <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <div class="cal">
                                                <div class="image-container">
                                                    <input type="hidden" class="user-id" id="val-avatar" value="<?= $avatar ?>" name="avatar">
                                                    <img src="<?= site_url('public/images/' . $avatar) ?>" loading="lazy" data-image="<?= site_url('public/images/' . $avatar) ?>">
                                                    <div class="file-input-wrapper">
                                                        <label for="avatar-upload" class="btn_upload">Thay đổi Avatar</label>
                                                        <input id="avatar-upload" type="file" name="fileUpload">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="val-skill">Permission <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <select class="form-control" name="permission" id="val-permission" name="val-skill">
                                                <!-- <option value="">Please select</option> -->
                                                <option value="0" <?php if ($permission == '0') {
                                                                        echo ' selected="selected"';
                                                                    } ?>>Admin</option>
                                                <option value="1" <?php if ($permission == '1') {
                                                                        echo ' selected="selected"';
                                                                    } ?>>User</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="val-skill">Address <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <select class="form-control" name="address" id="val-address">
                                                <?php foreach ($province  as $userAdmin) : extract($userAdmin); ?>
                                                    <?php if ($id == $user['address']) : ?>
                                                        <option value="<?php echo $id; ?>" selected><?php echo $name; ?></option>
                                                    <?php else : ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="val-password">Password <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <input type="password" class="form-control" id="val-password" name="password" placeholder="Choose a safe one..">
                                        </div>
                                    </div>

                                    <!-- end popup -->
                                    <div class="form-group row">
                                        <div class="col-lg-7 ml-auto">
                                            <button class="btn btn-primary btn-update">Submit</button>
                                            <button type="button" class="btn btn-danger">
                                                <a href="<?= base_url().'admin/userController' ?>" class="text-white">Cancel</a>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            <?php endforeach ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->
</div>