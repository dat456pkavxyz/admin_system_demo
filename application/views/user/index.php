<?php
$stt = 0; ?>

<!-- Page wrapper  -->
<div class="page-wrapper">

    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Manage Users</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Manage Users</a></li>
                <li class="breadcrumb-item active">List Users</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <div class="row page-titles mt-2">
        <div class="col-md-12 align-self-right">
            <ol class="breadcrumb">
                <div class="input-group">
                    <input class="form-control" id="keyword" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-info" type="button" id="searchInforUser">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
                <button type="button" class="btn btn-danger ml-2">
                    <a href="user/viewInsert" class="text-white">Create</a>
                </button>
            </ol>
        </div>
    </div>

    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Table</h4>

                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped" style="text-align: center;">
                                <thezad>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Province</th>
                                        <th>Phone</th>
                                        <th>Status</th>
                                        <th>Created_time</th>
                                        <th>Updated_time</th>
                                        <th class="text-center">Control</th>

                                    </tr>
                                    </thead>
                                    <!-- : extract($data) -->
                                    <tbody>
                                        <?php foreach ($num as $value) :;
                                            $stt = $offset + 1; // tính lại số thứ tự
                                            $offset++; // tăng biến $offset lên 1 sau mỗi bản ghi

                                        ?>
                                            <tr>
                                                <td><?php echo $stt; ?></td>
                                                <input type="text" name="id" class="user-id" value="<?php echo $value['id'] ?>" hidden>
                                                <td><?php echo $value['name'] ?></td>
                                                <td><?php echo $value['email'] ?></td>
                                                <td>
                                                    <?php foreach ($province  as $userAdmin) : extract($userAdmin); ?>
                                                        <?php if ($value['address'] == $userAdmin['id']) : ?>
                                                            <?php echo $name; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                                <td><?php echo $value['phone'] ?></td>
                                                <td><a href="#"><span class="badge <?= $value['status'] == 1 ? 'badge-success' : 'badge-danger' ?>"><?= $value['status'] == 1 ? 'Success' : 'Disable' ?></span></a></td>
                                                <td><?php echo $value['created_time'] ?></td>
                                                <td><?php echo $value['updated_time'] ?></td>
                                                <td class="text-center " id="text">
                                                    <div class="d-flex" style="color: white; ">
                                                        <button type="button" class="btn btn-info mr-1">
                                                            <a href="user/viewUpdate/<?php echo $value['id'] ?>" class="text-white get-id">
                                                                <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. -->
                                                                    <path d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.7 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z" />
                                                                </svg></a>
                                                        </button>
                                                        <button type="button" class="btn btn-danger btn-delete">
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512"><!--! Font Awesome Free 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. -->
                                                                <path d="M135.2 17.7L128 32H32C14.3 32 0 46.3 0 64S14.3 96 32 96H416c17.7 0 32-14.3 32-32s-14.3-32-32-32H320l-7.2-14.3C307.4 6.8 296.3 0 284.2 0H163.8c-12.1 0-23.2 6.8-28.6 17.7zM416 128H32L53.2 467c1.6 25.3 22.6 45 47.9 45H346.9c25.3 0 46.3-19.7 47.9-45L416 128z" />
                                                            </svg>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach;
                                        ?>

                                    </tbody>


                            </table>
                            <div class="pagination">
                                <a href="#" class="pagination__link pagination__link--prev">&laquo;</a>
                                <?php for ($i = 1; $i <= $total_pages; $i++) {
                                    $active_class = ($i == $current_page) ? 'pagination__link--active' : '';
                                    echo '<a class="pagination__link ' . $active_class . '" href="?page=' . $i . '">' . $i . '</a> ';
                                } ?>
                                <a href="#" class="pagination__link pagination__link--next">&raquo;</a>
                            </div>

                            <!-- <a href="#" class="pagination__link pagination__link--active">2</a> -->
                            <!-- <a href="#" class="pagination__link">3</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->