<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Manage Users</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">List User</a></li>
                <li class="breadcrumb-item active">Create User</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-validation">
                            <form class="form-valide" id="myForm" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="val-username">Username <span class="text-danger">*</span></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="val-name" name="name" placeholder="Enter a username..">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="val-email">Email <span class="text-danger">*</span></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="val-email" name="email" placeholder="Your valid email..">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="val-suggestions">Phone <span class="text-danger">*</span></label>
                                    <div class="col-lg-8">
                                        <input class="form-control" type="phone" id="val-phone" name="phone" rows="5" placeholder="phone...">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="val-avatar">Avatar <span class="text-danger">*</span></label>
                                    <div class="col-lg-8" id="myImg">
                                        <input type="file" id="val-img" name="fileUpload">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="val-skill">Permission <span class="text-danger">*</span></label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="permission" id="val-permisson">
                                            <!-- <option value="">Please select</option> -->
                                            <option value="0">Admin</option>
                                            <option value="1">User</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="val-skill">Address <span class="text-danger">*</span></label>
                                    <div class="col-lg-8">
                                        <select class="form-control form-control-line" name="address">
                                            <option value="">--- Choose Province ---</option>
                                            <?php foreach ($province  as $user) : extract($user); ?>
                                                <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="val-password">Password <span class="text-danger">*</span></label>
                                    <div class="col-lg-8">
                                        <input type="password" class="form-control" id="val-password" name="password" placeholder="Choose a safe one..">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6 ml-auto">
                                        <button class="btn btn-primary btn_add">Submit</button>
                                        <button type="button" class="btn btn-danger">
                                            <a href="<?= base_url().'admin/userController'?>" class="text-white">Cancel</a>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->
</div>
<!-- 