<?php
class authController extends CI_Controller
{
    public $form_validation;
    public $session;
    public $authModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('authModel');
        $this->load->library('session');
        $this->load->library('form_validation');
    }
    public function index()
    {
        if (!empty($this->session->userdata('currentuser'))) {
            redirect('admin', 'refresh');
        } else {
            $this->load->view('auth/login.php');
        }
    }
    public function login()
    {

        $data = $_POST;
        $this->form_validation->set_rules('email', 'Email', 'required|min_length[6]|max_length[32]');
        $this->form_validation->set_rules('password', 'Mật khẩu', 'required|min_length[6]|max_length[32]');
        if ($this->form_validation->run() == TRUE) {
            if (isset($_POST['login'])) {

                extract($data);
                $hash_pass = $_POST['password'];
                $pass = md5($hash_pass) . 'nguyenbadat';
                $res = $this->authModel->get_user($email, $pass)->result_array();
                if (!empty($res)) {
                    $this->session->set_userdata('currentuser', $res);
                    redirect('admin', 'refresh');
                } else {
                    $data['error'] = 'Sai mật khẩu hoặc tài khoản đăng nhập!';
                    $this->load->view('auth/login.php', $data);
                }
            }
        } else {
            $this->load->view('auth/login.php', $data);
        }
    }
    public function logout()
    {
        if (!empty($this->session->userdata('currentuser'))) {
            $this->session->unset_userdata('currentuser');
            redirect('admin/viewLogin', 'refresh');
        }
        else {
            echo('Lỗi ròi đấy! Fix');
        }
        
    }
}
