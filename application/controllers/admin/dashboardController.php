<?php
class dashboardController extends CI_Controller
{
    public $controlUser;
    public $controlProvince;
    public $session;
    public $productModel;
    public function __construct()
    {

        parent::__construct();
        $this->load->model('userModel');
        $this->load->model('provinceModel');
        $this->load->model('dashboardModel');
        $this->load->model('productModel');
        $this->controlProvince = new provinceModel();
        $this->controlUser = new userModel();
        if (!$this->session->userdata('currentuser')) {
            redirect('viewLogin', 'refresh');
        }
    }
    public function initialize()
    {
        $data = array();
        $data['dataUser'] = $this->controlUser->getAllUser()->result_array();
        $data['dataProduct'] = $this->productModel->getAllProduct()->result_array();
        $data['inforUserAdmin'] = $this->session->userdata('currentuser');
        $data['dataAdminCurrent'] = $this->controlUser->getbyId($data['inforUserAdmin'][0]['id'])->result_array();
        return $data;
    }
    public function index()
    {
        if (!empty($this->session->userdata('currentuser'))) {
            $data = $this->initialize();
            $data['subview'] = 'dashboard/index';
            $this->load->view('main.php', $data);
        }
    }
    public function profile()
    {
        $data['inforUserAdmin'] =  $this->session->userdata('currentuser');
        $id =  $data['inforUserAdmin'][0]['id'];
        if ($dataUser = $this->controlUser->getbyId($id)->result_array()) {
            $data = $this->initialize();
            $data['province'] = $this->controlProvince->getProvince()->result_array();
            $data['dataUser'] = $dataUser;
            $data['subview'] = 'dashboard/profile';
            $this->load->view('main.php', $data);
        } else {
            show_error('User not found');
        }
    }
}
