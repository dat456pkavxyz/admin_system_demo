<?php
class searchController extends CI_Controller
{
    public $searchM;
    public $session;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('searchModel');
        if (!$this->session->userdata('currentuser')) {
            redirect('viewLogin', 'refresh');
        }
    }
    public function search(){
        if(isset($_GET['type']) && $_GET['type'] == 'getUserKey'){
            $result = $_POST['keyword'];
            $res= $this->searchM->searchUser($result)->result_array();
            if ($res) {
                echo json_encode($res);
                return;
            }
            echo (false);
        } else {
            show_error('User not found');
        }
    }
   
}
