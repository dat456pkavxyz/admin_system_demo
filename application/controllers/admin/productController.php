<?php
class productController extends CI_Controller
{
    public $productModel;
    public $userModel;
    public $session;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('userModel');
        $this->load->model('productModel');
        $this->load->helper('response_helper');
        if (!$this->session->userdata('currentuser')) {
            redirect('viewLogin', 'refresh');
        }
    }
    public function initialize()
    {
        $data = array();
        $data['inforUserAdmin'] = $this->session->userdata('currentuser');
        $data['dataAdminCurrent'] = $this->userModel->getbyId($data['inforUserAdmin'][0]['id'])->result_array();
        return $data;
    }
    public function index()
    {
        $data = $this->initialize();
        // Lấy trang hiện tại
        $data['current_page'] = isset($_GET['page']) ? $_GET['page'] : 1;           //start pagination
        // Lấy số bản ghi và số trang cần hiển thị
        $records_per_page = 8;
        $total_records = $this->productModel->countProduct();
        $data['total_pages'] = ceil($total_records / $records_per_page);
        // Tính offset và limit để lấy bản ghi của từng trang
        $data['offset'] = ($data['current_page'] - 1) * $records_per_page;
        $limit = $records_per_page;
        $data['num'] = $this->productModel->total_a_pageProduct($data['offset'], $limit);     //end pagination
        $data['image_product'] = $this->productModel->getAllImage()->result_array();
        $data['subview'] = 'product/index';
        $this->load->view('main.php', $data);
    }
    public function viewInsertP()
    {
        $data = $this->initialize();
        $data['subview'] = 'product/create';
        $this->load->view('main.php', $data);
    }
    public function viewUpdateP($id)
    {
        $data = $this->initialize();
        if ($dataUser = $this->productModel->getbyId($id)->result_array()) {
            $data['subview'] = 'product/update';
            $data['dataUser'] = $dataUser;
            $this->load->view('main.php', $data);
        } else {
            show_error('User not found');
        }
    }
    public function insert()
    {
        // echo('dkbfjs');
        if (!empty($_POST)) {
            $data = $_POST['data'];
            $dataObj = json_decode($data, true);
            extract($dataObj);
            $image_product =  $_FILES['fileUpload']['name'];
            move_uploaded_file($_FILES['fileUpload']['tmp_name'], 'public/images/uploadIMG/' . $_FILES['fileUpload']['name']);
            $resData = array(
                'name' => $_POST['name'],
                'description' => $description,
                'supplier' => $_POST['sup'],
                'countInStock' => $_POST['stock'],
                'discount' => $_POST['discount'],
                'price' => $_POST['price'],
                'status' => $_POST['status'],
                'img' => $image_product,
            );
            print_r($resData);
            $res = $this->productModel->addProduct($resData);
            if ($res) {
                echo (true);
                return;
            }
            echo (false);
        }
        echo (false);
    }

    public function upload()
    {
        if (isset($_FILES['file']) && $_FILES['file']['name'] != '') {
            $file_names = '';
            $total = count($_FILES['file']['name']);
            for ($i = 0; $i < $total; $i++) {
                $filename = $_FILES['file']['name'][$i];
                $extension = pathinfo($filename, PATHINFO_EXTENSION);
                $valid_extensions = array("png", "jpg", "jpeg", "avif");
                if (in_array($extension, $valid_extensions)) {
                    $path = 'public/images/uploadIMG/' . $filename;
                    move_uploaded_file($_FILES['file']['tmp_name'][$i], $path);
                    $file_names .=  $filename . ".";
                    $resIMG = array(
                        'productimage' => $filename,
                        'idproduct' => '̣'
                    );
                    $resImage = $this->productModel->addProductIMG($resIMG);
                    if ($resImage) {
                        return;
                    }
                    echo (false);
                } else
                    echo (false);
            }
        }
    }

    public function delete()
    {
        if (!empty($_POST)) {
            $data = $_POST;
            $res = $this->productModel->deleteProduct($_POST['id']);
            if($res){
                $ojb =ojbResponse(true,"Delete success");
             }else{
                $ojb =ojbResponse(false,"Delete do not  success");
             }
             echo json_encode($ojb);
        }
    }
}
