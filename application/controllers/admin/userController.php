<?php
class userController extends CI_Controller
{
    public $session;
    public $userModel;
    public $provinceModel;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('userModel');
        $this->load->model('provinceModel');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('response_helper');
        if (!$this->session->userdata('currentuser')) {
            redirect('viewLogin', 'refresh');
        }
    }
    public function initialize()
    {
        $data = array();
        $data['inforUserAdmin'] = $this->session->userdata('currentuser');
        $data['dataAdminCurrent'] = $this->userModel->getbyId($data['inforUserAdmin'][0]['id'])->result_array();
        $data['province'] = $this->provinceModel->getProvince()->result_array();
        return $data;
    }
    public function index()
    {

        $data = $this->initialize();
        // Lấy trang hiện tại
        $data['current_page'] = isset($_GET['page']) ? $_GET['page'] : 1;           //start pagination
        // Lấy số bản ghi và số trang cần hiển thị
        $records_per_page = 10;
        $total_records = $this->userModel->countUser();
        $data['total_pages'] = ceil($total_records / $records_per_page);
        // Tính offset và limit để lấy bản ghi của từng trang
        $data['offset'] = ($data['current_page'] - 1) * $records_per_page;
        $limit = $records_per_page;
        $data['num'] = $this->userModel->total_a_page($data['offset'], $limit);     //end pagination
        $data['subview'] = 'user/index';
        $this->load->view('main.php', $data);
    }
    public function viewInsert()
    {
        $data = $this->initialize();
        $data['subview'] = 'user/create_user';
        $this->load->view('main.php', $data);
    }
    public function viewUpdate($id)
    {
        $data = $this->initialize();
        if ($dataUser = $this->userModel->getbyId($id)->result_array()) {
            $data['subview'] = 'user/update_user';
            $data['dataUser'] = $dataUser;
            $this->load->view('main.php', $data);
        } else {
            show_error('User not found');
        }
    }
    public function insert()
    {
        if (!empty($_POST)) {
            // print_r(json_encode($_POST));
            $data = $_POST;
            extract($data);
            $findUser = $this->userModel->getbyId($_POST['id']);
            // extract($_FILES); ở $_FILES này sẽ chứa tên ảnh
            $avatar =  $_FILES['fileUpload']['name'];
            move_uploaded_file($_FILES['fileUpload']['tmp_name'], 'public/images/uploadIMG/' . $_FILES['fileUpload']['name']);
            $time = date("Y-m-d H:i:s");
            $hash_pass = md5($_POST['password']) . 'nguyenbadat';
            $resData = array('name' => $name, 'email' => $email, 'address' => $address, 'avatar' => $avatar, 'phone' => $phone, 'password' => $hash_pass, 'created_time' => $time, 'updated_time' => $time, 'status' => '1', 'permission' => $permission);
            $res = $this->userModel->addUser($resData);
            if ($res) {
                echo (true);
                return;
            }
            echo (false);
        }
        echo (false);
    }
    public function delete()
    {
        if (!empty($_POST)) {
            $data = $_POST;
            $res = $this->userModel->deleteUser($_POST['id']);
            if($res){
                $ojb =ojbResponse(true,"Delete success");
             }else{
                $ojb =ojbResponse(false,"Delete do not  success");
             }
             echo json_encode($ojb);
        }
    }
    public function update()
    {
        if (!empty($_POST)) {
            $data_request = $_POST;
            extract($data_request);
            //check khi không lất ảnh mới ở ô input file thì sẽ lấy tên ảnh ở giá trị cũ
            if (!empty($_POST['type'] == 'updateUser')) {
                $avatar = $_POST['avatar'];
            } else {
                $avatar =  $_FILES['fileUpload']['name'];
                move_uploaded_file($_FILES['fileUpload']['tmp_name'], 'public/images/' . $_FILES['fileUpload']['name']);
            }
            //check khi người dùng không nhập mật khẩu vào ô input và muốn dùng lại mật khẩu cũ
            $time = date("Y-m-d H:i:s");
            $findUser = $this->userModel->getbyId($_POST['id'])->result_array();
            if (empty($_POST['password'])) {
                $hash_pass = $findUser[0]['password'];
            } else {
                $hash_pass = md5($_POST['password']) . 'nguyenbadat';
            }
            $res = array('name' => $name, 'email' => $email, 'address' => $address, 'avatar' => $avatar, 'phone' => $phone, 'password' => $hash_pass, 'updated_time' => $time, 'status' => '1', 'permission' => $permission);
            $resData = $this->userModel->updateUser($id, $res);
            echo ($resData);
            if ($resData) {
                echo (true);
                return;
            }
            echo (false);
        }
    }
    public function search(){
        if(isset($_POST['type']) && $_POST['type'] == 'getUserKey'){
            $result = $_POST['keyword'];
            $res= $this->userModel->searchUser($result)->result_array();
            if ($res) {
                echo json_encode($res);
                return;
            }
            echo (false);
        } else {
            show_error('User not found');
        }
    }
}
