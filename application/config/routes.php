<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
// //dashboard
// $route['admin']= 'admin/dashboardController/index';
// $route['profile']= 'admin/dashboardController/profile';
// //auth
// $route['viewLogin'] = 'admin/authController/index';
// $route['login'] = 'admin/authController/login';
// $route['logout'] = 'admin/authController/logout';
// //user
// // view
// $route['viewUser'] = 'admin/userController';
// $route['viewInsert'] = 'admin/userController/viewInsert';
// $route['viewUpdate/(:num)'] = 'admin/userController/viewUpdate/$1';
// $route['search/(:num)'] = 'admin/userController/search/$1';
// // action
// $route['insertUser'] = 'admin/userController/insert';
// $route['updateUser'] = 'admin/userController/update';
// //
// $route['viewProduct'] = 'admin/productController/index';

//
// Route Group cho User
$route['admin/user'] = 'admin/userController';
$route['admin/user/view/(:num)'] = 'admin/userController/view/$1';
$route['admin/user/insert'] = 'admin/userController/insert';
$route['admin/user/update'] = 'admin/userController/update';
$route['admin/user/search/(:num)'] = 'admin/userController/search/$1';
$route['admin/user/viewInsert'] = 'admin/userController/viewInsert';
$route['admin/user/viewUpdate/(:num)'] = 'admin/userController/viewUpdate/$1';

// Route cho Auth
$route['admin'] = 'admin/dashboardController/index';
$route['admin/profile'] = 'admin/dashboardController/profile';
$route['admin/viewLogin'] = 'admin/authController/index';
$route['admin/login'] = 'admin/authController/login';
$route['admin/logout'] = 'admin/authController/logout';

// Route cho Product
$route['admin/product'] = 'admin/productController/index';
$route['admin/product/viewInsertP'] = 'admin/productController/viewInsertP';
$route['admin/product/insert'] = 'admin/productController/insert';
$route['admin/product/viewUpdateP/(:num)'] = 'admin/productController/viewUpdateP/$1';


